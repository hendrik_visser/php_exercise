<?php

$fruits = array("d" => "citroen", "a" => "sinasappel", "b" => "banaan", "c" => "appel");

function test_alter(&$item1, $key, $prefix)
{
    $item1 = "$prefix: $item1";
}

function test_print($item2, $key)
{
    echo "$key. $item2<br />\n";
}
echo "Voor ...:\n";
array_walk($fruits, 'test_print');

array_walk($fruits, 'test_alter', 'fruit');
echo "... en na:\n";

array_walk($fruits, 'test_print');