<?php

class Myclass
{
	static $var = "Ik ben de static variable";
	public static function staticMethod()
	{
		echo "Ik ben de static method";
	}
}

Myclass::staticMethod();

//Let op bij het ophalen van een static variable met wel $
echo Myclass::$var;
